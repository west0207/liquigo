create table test_sql_file (
  id varchar2(36) not null,
  role_name varchar2(50),
  detail varchar2(250),
  app_id varchar2(36),
  constraint pk_test_sql_file primary key (id)
);
drop table test_sql_file cascade constraints;
