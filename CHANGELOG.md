# Changelog

This changelog goes through all the changes that have been made in each release
without substantial changes to our git log.

## [0.2.17](https://gitee.com/west0207/liquigo/releases/tag/0.2.17) - 2022-03-13

* ENHANCEMENTS

  * Optimize README.md