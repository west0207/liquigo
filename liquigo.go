// Copyright 2022 The Liquigo Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	config "gitee.com/west0207/liquigo/core/config"
	handle "gitee.com/west0207/liquigo/core/handle"
	. "gitee.com/west0207/liquigo/core/log"
)

// 入口函数
func main() {
	flag.BoolVar(&config.XormLog, "x", false, "显示中间件xorm的Debug信息")
	flag.BoolVar(&config.DryRun, "d", false, "空转，仅输出sql文本")
	flag.BoolVar(&config.Version, "v", false, "版本号")

	// var username string
	// flag.StringVar(&username, "u", "", "用户名，默认为空")
	flag.Parse()
	if config.Version {
		fmt.Printf("liquigo v%v\n", config.LiquigoVersion)
		os.Exit(0)
	}
	Run()
}

// 运行入口，外部应用可以调用该方法运行
func Run() {
	InitLog()
	Sug.Infof("liquigo start")
	if err := handle.Handle(config.YamlConfig); err != nil {
		if strings.HasPrefix(err.Error(), "liquigo stopped") {
			Sug.Info(err.Error())
		} else {
			Sug.Errorf("%v", err)
		}
	}
	Sug.Infof("liquigo end")
}
