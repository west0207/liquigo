// Copyright 2022 The Liquigo Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package translate

import (
	utils "gitee.com/west0207/liquigo/core/utils"

	etree "gitee.com/west0207/etree"
)

// <dropView viewName="v_test_role" dropIfExists="true"/>
// 获取删除视图的Entity标签的sql语句
// dbmsName *string 数据库类型名称，例如：mysql
// dropViewEle *etree.Element 删除视图的Entity标签
func GetDropViewSql(dbmsName *string, dropViewEle *etree.Element) (string, error) {
	viewName := dropViewEle.SelectAttrValue("viewName", utils.EMPTY)
	SetPropertyValue(dbmsName, &viewName)
	dropIfExists := dropViewEle.SelectAttrValue("dropIfExists", utils.FALSE)

	if *dbmsName == Oracle {
		// Oracle不支持if exists
		sql := "drop view " + viewName + ";"
		return sql, nil
	}
	if dropIfExists == utils.TRUE {
		sql := "drop view if exists " + viewName + ";"
		return sql, nil
	}
	sql := "drop view " + viewName + ";"
	return sql, nil
}
