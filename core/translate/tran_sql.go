// Copyright 2022 The Liquigo Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package translate

import (
	utils "gitee.com/west0207/liquigo/core/utils"

	etree "gitee.com/west0207/etree"
)

// 获取sql的Entity标签的sql语句
// dbmsName *string 数据库类型名称，例如：mysql
// sqlEle *etree.Element sql的Entity标签
func GetSqlSql(dbmsName *string, sqlEle *etree.Element) (string, error) {
	// splitStatements := sqlEle.SelectAttrValue("splitStatements", utils.EMPTY)
	// stripComments := sqlEle.SelectAttrValue("stripComments", utils.EMPTY)

	// fmt.Println(sqlEle.Text())
	return sqlEle.Text() + utils.LF, nil
}
