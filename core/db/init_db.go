// Copyright 2022 The Liquigo Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package db

import (
	. "gitee.com/west0207/liquigo/core/log"

	config "gitee.com/west0207/liquigo/core/config"

	xormlog "xorm.io/xorm/log"

	_ "gitee.com/chunanyong/dm" // 达梦

	_ "github.com/denisenkom/go-mssqldb" // SQL Server
	_ "github.com/go-sql-driver/mysql"   // MySQL,MariaDB,TiDB
	_ "github.com/godror/godror"         // Oracle
	_ "github.com/jmrobles/h2go"         // H2仅用于测试
	_ "github.com/lib/pq"                // PostgreSQL
	_ "kingbase.com/gokb"                // 人大金仓
	_ "modernc.org/sqlite"               // sqlite
	"xorm.io/xorm"
)

var Engine *xorm.Engine

// 初始化链接
// dbConfig *config.DB 一个数据库配置
func InitDb(dbConfig *config.DB) {
	var err error
	Engine, err = xorm.NewEngine(dbConfig.DriverName, dbConfig.DataSourceName)
	// Sug.Infof("Engine %v %T , err %v", Engine, Engine, err)
	if err != nil {
		// 创建数据库连接失败
		Sug.Errorf("Failed to create database connection, err: %v", err)
		Sug.Errorf("Database connection configuration: %v", dbConfig)
		panic(err)
	}

	if config.XormLog {
		// 在控制台打印出生成的SQL语句
		Engine.ShowSQL(true)
		// 在控制台打印调试及以上的信息
		Engine.Logger().SetLevel(xormlog.LOG_DEBUG)
	}

	// 最大连接数
	Engine.SetMaxOpenConns(2)
	// 闲置连接数
	Engine.SetMaxIdleConns(1)

	// // 打开连接失败
	// dbConn, err := sql.Open(dbConfig.DriverName, dbConfig.DataSourceName)
	// if err != nil {
	// 	// 创建数据库连接失败
	// 	Sug.Errorf("Failed to create database connection, err: %v", err)
	// 	Sug.Errorf("Database connection configuration: %v", dbConfig)
	// 	panic(err)
	// }

	// // defer dbConn.Close()

	// // 最大连接数
	// dbConn.SetMaxOpenConns(10)
	// // 闲置连接数
	// dbConn.SetMaxIdleConns(5)
	// // 最大连接周期
	// dbConn.SetConnMaxLifetime(100 * time.Second)

	// if err = dbConn.Ping(); nil != err {
	// 	// 检测数据库连接失败
	// 	Sug.Errorf("Failed to detect database connection, err: %v", err)
	// 	panic(err)
	// }
	// return dbConn, nil
}
