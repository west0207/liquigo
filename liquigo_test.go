// Copyright 2022 The Liquigo Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"strings"
	"testing"

	handle "gitee.com/west0207/liquigo/core/handle"
	. "gitee.com/west0207/liquigo/core/log"
)

func TestMain(m *testing.M) {
	InitLog()
	Sug.Infof("liquigo start")
	if err := handle.Handle("app_test.yml"); err != nil {
		if strings.HasPrefix(err.Error(), "liquigo stopped") {
			Sug.Info(err.Error())
		} else {
			Sug.Errorf("%v", err)
		}
	}
	Sug.Infof("liquigo end")
}
